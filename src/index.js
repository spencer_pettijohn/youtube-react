import _ from 'lodash';
import React, { Component, Children } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import VideoList from './Components/video_list';
import VideoDetail from './Components/video_detail';
const API_KEY = 'AIzaSyBtuSnzKlCtx4aLi3SBr_JArSUFCiNzUNM';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.videoSearch('homes in Dallas');
  }

  videoSearch(term) {
    YTSearch({key: API_KEY, term: term}, (videos) => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
      });
    });
  }
  componentDidMount(){
  };

  render() {

    const videoSearch = _.debounce((term) => {
      this.videoSearch(term)
    }, 300);
    return (
        <div className='enlarge'>
          <VideoDetail video={this.state.selectedVideo}/>

          <VideoList
              onVideoSelect={selectedVideo => this.setState({selectedVideo}) }
              videos={this.state.videos}/>
        </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector('.container'));
